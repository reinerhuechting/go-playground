package generics

import (
	"fmt"
)

// Function for test purposes.
// Expects an int x and returns 2*x.
func Times2(x int) int {
	return 2 * x
}

// Non generic function that expects a func(int) int, calls it, and prints the result.
func CallAndPrintInt(f func(int) int, arg int) {
	fmt.Println(f(arg))
}

// Function for test purposes.
// Expects a string s and returns a string, where s is s appended to itself.
func StringTwice(s string) string {
	return s + s
}

// Non generic function that expects a func(string) string, calls it, and prints the result.
func CallAndPrintString(f func(string) string, arg string) {
	fmt.Println(f(arg))
}

// Generic function that expects a func(T) T, calls it, and prints the result.
func CallAndPrintGeneric[T any](f func(T) T, arg T) {
	fmt.Println(f(arg))
}

// Function for test purposes.
// Expects a string s and a number n. Returns a string, where s is repeated n times.
func TimesN(s string, n int) string {
	if n == 0 {
		return ""
	}
	return s + TimesN(s, n-1)
}

// Non-Generic function that expects a func(string int) string, calls it, and prints the result.
func CallAndPrintStringInt(f func(string, int) string, argString string, argInt int) {
	fmt.Println(f(argString, argInt))
}

// Interface that allows either a number or a string.
type IntOrString interface {
	int | string
}

// Generic function that expects a func with two possibly different parameter types,
// calls it, and prints the result.
func CallAndPrintTwoParamGeneric[T1, T2 IntOrString](f func(T1, T2) T1, arg1 T1, arg2 T2) {
	fmt.Println(f(arg1, arg2))
}

// Function for test purposes.
// Expects a string parameter pack and concatenates all of its strings.
func ConcatAll(strings ...string) string {
	result := ""
	for _, s := range strings {
		result += s
	}
	return result
}

// Non-Generic function that expects a func(...string) string,
// calls it, and prints the result.
func CallAndPrintVariadicString(f func(...string) string, args ...string) {
	fmt.Println(f(args...))
}

// Function for test purposes.
// Expects an int parameter pack and adds up all of its strings.
func AddAll(strings ...int) int {
	result := 0
	for _, s := range strings {
		result += s
	}
	return result
}

// Generic function that expects a func(...T) T,
// calls it, and prints the result.
func CallAndPrintVariadicGeneric[T any](f func(...T) T, args ...T) {
	fmt.Println(f(args...))
}

// Struct that stores a binary int function.
type IntFuncContainer struct {
	f func(int) int
}

// Method for IntFuncContainer that calls and prints the stored function.
func (b *IntFuncContainer) callAndPrint(arg int) {
	fmt.Println(b.f(arg))
}

// Generic struct for a unary function.
type UnaryFunctionContainer[ArgType, ResultType any] struct {
	f func(ArgType) ResultType
}

// Method for FuncContainer that calls and prints the stored function.
func (b *UnaryFunctionContainer[ArgType, ResultType]) callAndPrint(arg ArgType) {
	fmt.Println(b.f(arg))
}

// Constraints for binary function types.
type BinaryFuncConstraints interface {
	func(int) int | func(string) string
}

// Generic container for functions.
type GenericBinaryFunctionContainer[FuncType BinaryFuncConstraints] struct {
	f FuncType
}

type GenericFuncConstraints[ArgType, ResultType any] interface {
	func(ArgType) ResultType
}

type GenericFunctionContainer[ArgType, ResultType any, FuncType GenericFuncConstraints[ArgType, ResultType]] struct {
	f FuncType
}
