package generics

import "fmt"

func ExampleCallAndPrintInt() {
	CallAndPrintInt(Times2, 4)

	// Output:
	// 8
}

func ExampleCallAndPrintString() {
	CallAndPrintString(StringTwice, "Ha")

	// Output:
	// HaHa
}

func ExampleCallAndPrintGeneric() {
	CallAndPrintGeneric(Times2, 4)
	CallAndPrintGeneric(StringTwice, "Ha")

	// Output:
	// 8
	// HaHa
}

func ExampleCallAndPrintStringInt() {
	CallAndPrintStringInt(TimesN, "Ha", 5)

	// Output:
	// HaHaHaHaHa
}

func ExampleCallAndPrintTwoParamGeneric() {
	CallAndPrintTwoParamGeneric(TimesN, "Ha", 5)

	// Output:
	// HaHaHaHaHa
}

func ExampleCallAndPrintVariadicString() {
	CallAndPrintVariadicString(ConcatAll, "Ha", "He", "Hi")

	// Output:
	// HaHeHi
}

func ExampleCallAndPrintVariadicGeneric() {
	CallAndPrintVariadicGeneric(ConcatAll, "Ha", "He", "Hi")
	CallAndPrintVariadicGeneric(AddAll, 1, 2, 3)

	// Output:
	// HaHeHi
	// 6
}

func ExampleIntFuncContainer() {
	container := IntFuncContainer{Times2}
	container.callAndPrint(21)

	// Output:
	// 42
}

func ExampleUnaryFunctionContainer() {
	container1 := UnaryFunctionContainer[int, int]{Times2}
	container1.callAndPrint(21)

	container2 := UnaryFunctionContainer[string, string]{StringTwice}
	container2.callAndPrint("Ho")

	// Output:
	// 42
	// HoHo
}

func ExampleGenericBinaryFunctionContainer() {
	container1 := GenericBinaryFunctionContainer[func(int) int]{Times2}
	fmt.Println(container1.f(21))

	container2 := GenericBinaryFunctionContainer[func(string) string]{StringTwice}
	fmt.Println(container2.f("Hi"))

	// Output:
	// 42
	// HiHi
}

func ExampleGenericFunctionContainer() {
	container1 := GenericFunctionContainer[int, int, func(int) int]{Times2}
	fmt.Println(container1.f(21))

	container2 := GenericFunctionContainer[string, string, func(string) string]{StringTwice}
	fmt.Println(container2.f("Hi"))

	// Output:
	// 42
	// HiHi
}
